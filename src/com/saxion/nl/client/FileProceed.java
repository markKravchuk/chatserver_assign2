package com.saxion.nl.client;

import com.saxion.nl.messages.Message;
import com.saxion.nl.messages.MessageHandler;
import com.saxion.nl.messages.actual_mesages.AuthenticationMessage;
import com.saxion.nl.messages.actual_mesages.errors.BaseErrorMessage;
import com.saxion.nl.messages.actual_mesages.file.SendFileMessage;
import com.saxion.nl.messages.actual_mesages.file.SendFileResponse;

import java.io.*;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileProceed extends Thread {

    private InputStream inputStream;
    private BufferedReader reader;
    private OutputStream outputStream;
    private PrintWriter writer;

    private File fileToSend;

    private String username;
    private SendFileMessage message;
    //place where received file will be saved
    private static final String RECEIVING_DIRECTORY = "D:\\java\\receive\\";

    FileProceed(String username, SendFileMessage message) {
        try {
            this.username = username;
            this.message = message;
            Socket socket = new Socket("127.0.0.1", 1338);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream));
            writer = new PrintWriter(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            AuthenticationMessage authenticationMessage = new AuthenticationMessage(username);
            writer.println(authenticationMessage.send());
            writer.flush();

            String output = reader.readLine();

            if (!output.contains("+OK HELO")) {
                System.err.println("SOMETHING WENT WRONG");
            }

            //send to a server a send file message with file details in it
            writer.println(message.send());
            writer.flush();

            //if client is a receiver
            if (fileToSend == null) {
                downloadFile();
            } else {
                uploadFile();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                this.join();
                System.out.println("[File sender] file connection closed");
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void downloadFile() {
        FileOutputStream fos;
        BufferedOutputStream bos;
        try {
            long fileLength = message.getSize();

            String filePath = RECEIVING_DIRECTORY + username + "-" + message.getFilename();

            boolean receivedCorrectly = false;
            //bytes array of file
            byte[] myByteArray = new byte[(int) fileLength];

            do {
                inputStream.read(myByteArray, 0, myByteArray.length);
                byte[] encodedHash = new byte[0];
                try {
                    MessageDigest digest = MessageDigest.getInstance("SHA-256");
                    encodedHash = digest.digest(myByteArray);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }

                String hashCode = hashedCode(encodedHash);

                if (hashCode.equals(message.getChecksum())) {  //checks if hashes are equal, continues to send the request until hashcodes are the same
                    writer.println(new SendFileResponse(true).send());
                    writer.flush();
                    receivedCorrectly = true;
                } else {
                    writer.println(new SendFileResponse(false).send());
                    writer.flush();
                }
            } while (!receivedCorrectly);

            fos = new FileOutputStream(filePath);
            bos = new BufferedOutputStream(fos);

            bos.write(myByteArray, 0, myByteArray.length);
            bos.flush();
            System.out.println("[File receiver] File " + filePath
                    + " downloaded (" + myByteArray.length + " bytes length)");
            fos.close();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void uploadFile() {
        BufferedInputStream bis;
        try {
            byte[] myByteArray = new byte[(int) fileToSend.length()];
            FileInputStream fis = new FileInputStream(fileToSend);
            bis = new BufferedInputStream(fis);
            bis.read(myByteArray, 0, myByteArray.length);

            boolean isCorrect = false;

            do {
                System.out.println("[File sender] Sending " + fileToSend.getName() + "(" + myByteArray.length
                        + " bytes) to " + message.getReceiver());
                outputStream.write(myByteArray, 0, myByteArray.length);
                outputStream.flush();

                String responseStr = reader.readLine();
                Message response = MessageHandler.getTypeMessage(responseStr);
                if (response instanceof SendFileResponse) {
                    if (((SendFileResponse) response).isCorrect()) isCorrect = true;
                } else if (response instanceof BaseErrorMessage) {
                    System.out.println("Error: " + response.send());
                }
            } while (!isCorrect);


            System.out.println("[File sender] Done.");
            bis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void setFileToSend(File fileToSend) {
        this.fileToSend = fileToSend;
    }


    static String hashedCode(byte[] encodedHash) {
        StringBuffer hexString = new StringBuffer();
        for (byte hash : encodedHash) {
            String hex = Integer.toHexString(0xff & hash);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

}

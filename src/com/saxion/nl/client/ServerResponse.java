package com.saxion.nl.client;

import com.saxion.nl.messages.Message;
import com.saxion.nl.messages.actual_mesages.AuthenticationMessage;
import com.saxion.nl.messages.actual_mesages.WelcomeMessage;
import com.saxion.nl.messages.actual_mesages.errors.BaseErrorMessage;
import com.saxion.nl.messages.actual_mesages.file.SendFileMessage;
import com.saxion.nl.messages.actual_mesages.groups.*;
import com.saxion.nl.messages.actual_mesages.ping_pong.PingMessage;
import com.saxion.nl.messages.actual_mesages.ping_pong.PongMessage;
import com.saxion.nl.messages.actual_mesages.users.BroadcastMessage;
import com.saxion.nl.messages.actual_mesages.users.RequestUsersMessage;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

class ServerResponse {
    private PrintWriter writer;
    private ClientLauncher parent;
    private ArrayList <FileProceed> fileSockets;

    ServerResponse(PrintWriter writer, ClientLauncher parent) {
        this.writer = writer;
        this.parent = parent;
        fileSockets = new ArrayList<>();
    }

    void handleResponse(Message message, String line) {
        if (message instanceof CreateGroupMessage) {
            System.out.println("Group created!");
        } else if (message instanceof AuthenticationMessage) {
            System.out.println("Hello, " + line.substring(9));
        } else if (message instanceof WelcomeMessage) {
            System.out.println(line.substring(5));
        } else if (message instanceof PingMessage) {
            writer.println(new PongMessage().send());
            writer.flush();
        } else if (message instanceof RequestUsersMessage) {
            if(line.length() < 22) System.out.println("No one is online");
            else System.out.println("Online users: " + line.substring(21));
        } else if (message instanceof GroupMessage) {
            if(message.isResponse()) System.out.println("Message was sent successfully");
            else System.out.println(((GroupMessage) message).getGroupName() + " (group) "
                    + ((GroupMessage) message).getSender() + " said: " + message.getMessage());
        } else if (message instanceof JoinGroupMessage) {
            System.out.println("Welcome to the group!");
        } else if (message instanceof KickUserMessage) {
            System.out.println("User was kicked!");
        } else if (message instanceof LeaveGroupMessage) {
            System.out.println("You left the group!");
        } else if (message instanceof RemoveGroupMessage) {
            System.out.println("Group was deleted!");
        } else if (message instanceof BaseErrorMessage) {
            System.err.println(line.substring(5));
        } else if (message instanceof BroadcastMessage) {
            if (message.isResponse()) System.out.println("Message was sent!");
            else System.out.println(((BroadcastMessage) message).getUsername() + " said to everyone: " + message.getMessage());
        } else if (message instanceof SendFileMessage) {
            if (message.getPrefix().startsWith("+OK")) {
                setUpFileReceiving((SendFileMessage) message, message);
            } else if (((SendFileMessage) message).isNegativeResponse()) {
                System.err.println(((SendFileMessage) message).getSender() + " rejected receiving file");
            } else {
                parent.addSendFileMessage((SendFileMessage) message);
            }
        } else if (message instanceof RequestGroupsMessage) {
            System.out.println("Groups available: " + message.getMessage());
        }
    }

    private void setUpFileReceiving(SendFileMessage sendFileMessage, Message message) {
        sendFileMessage.swapSenderAndReceiver();
        sendFileMessage.setPrefix(message.getPrefix().substring(4));
        File fileToSend = parent.getFile(message.send());

        if (fileToSend == null) {
            System.err.println("File to be sent to: " + sendFileMessage.getReceiver() + " can not be found");
        }
        FileProceed fileProceed = new FileProceed(parent.getUsername(), sendFileMessage);
        fileProceed.setFileToSend(fileToSend);

        fileProceed.start();
        fileSockets.add(fileProceed);
    }
}

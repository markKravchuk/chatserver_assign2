package com.saxion.nl.client;

import com.saxion.nl.messages.Message;
import com.saxion.nl.messages.actual_mesages.*;
import com.saxion.nl.messages.actual_mesages.file.SendFileMessage;
import com.saxion.nl.messages.actual_mesages.users.*;

import java.io.*;
import java.net.Socket;
import java.security.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Scanner;

public class ClientLauncher {
    private PrintWriter writer;
    private boolean usernameCorrect;
    private HashMap<String, File> files;
    private ArrayList<SendFileMessage> sendFileRequests;
    private ArrayList<PersonalMessage> messagesToSend;

    private Socket socket;

    private String username;

    private Key privateKey;

    public static void main(String[] args) {
        try {
            new ClientLauncher().run();
        } catch (IOException | InterruptedException e) {
            System.err.println("Connection with server lost");
            System.exit(0);
        }
    }

    private void run() throws IOException, InterruptedException {
        //variables responsible for connection socket & server
        socket = new Socket("127.0.0.1", 1337);
        OutputStream outputStream = socket.getOutputStream();
        writer = new PrintWriter(outputStream);
        Scanner scanner = new Scanner(System.in);
        files = new HashMap<>();
        sendFileRequests = new ArrayList<>();
        messagesToSend = new ArrayList<>();
        MenuHandler menuHandler = new MenuHandler(this, new PrintWriter(socket.getOutputStream()), username);

        PublicKeyMessage publicKeyMessage = null;

        try {
            // --- SECURITY ---

            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            //key size more than 1024 is really secure
            kpg.initialize(2048);
            KeyPair kp = kpg.generateKeyPair();

            Key publicKey = kp.getPublic();
            privateKey = kp.getPrivate();

            MessageReceiverThread messageReceiverThread = new MessageReceiverThread(socket, privateKey, this);
            messageReceiverThread.start();

            // keys initialized

            publicKeyMessage = new PublicKeyMessage(Base64.getEncoder().encodeToString(publicKey.getEncoded()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String userInput;

        do {
            System.out.println("Enter your username:");
            userInput = scanner.nextLine();

            AuthenticationMessage heloMessage = new AuthenticationMessage(userInput);
            sendMessage(heloMessage);

            username = userInput;

            Thread.sleep(200);

        } while (!usernameCorrect);

        menuHandler.setUsername(username);

        if (publicKeyMessage != null) sendMessage(publicKeyMessage);

        while (socket.isConnected()) {
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //check for incoming send file requests
            if (!sendFileRequests.isEmpty()) {
                proceedSendFileMessage(sendFileRequests.get(0));
                sendFileRequests.remove(0);
                continue;
            }

            menuHandler.handleMenu();
        }
    }

    synchronized ArrayList<PersonalMessage> getMessagesToSend() {
        return messagesToSend;
    }

    synchronized void addMessageToSend(PersonalMessage message) {
        messagesToSend.add(message);
    }

    synchronized PersonalMessage getMessageToSend(String username) {
        for (PersonalMessage pm : messagesToSend) {
            if (pm.getReceiver().equals(username)) {
                return pm;
            }
        }
        return null;
    }

    private void sendMessage(Message message) {
        writer.println(message.send());
        writer.flush();
    }

    private void proceedSendFileMessage(SendFileMessage message) {
        System.out.println("Would you like to receive a file " + message.getFilename()
                + " (" + message.getSize() + " bytes) from "
                + message.getSender() + " ?");
        Scanner scanner = new Scanner(System.in);

        Boolean userAgreed = null;

        do {
            String answer = scanner.nextLine();
            if (answer.toLowerCase().equals("yes") | answer.toLowerCase().equals("y")) {
                userAgreed = true;
            } else if (answer.toLowerCase().equals("no") | answer.toLowerCase().equals("n")) {
                userAgreed = false;
            } else {
                System.out.println("Enter please (y)es or (n)o");
            }
        } while (userAgreed == null);

        if (userAgreed) {
            message.setResponse(true);
            message.swapSenderAndReceiver();
            FileProceed fileProceed = new FileProceed(username, message);
            fileProceed.start();
        } else {
            message.rejectFile();
        }
        sendMessage(message);
    }

    synchronized File getFile(String message) {
        return files.get(message);
    }

    synchronized void addFile(String message, File file) {
        files.put(message,file);
    }

    public String getUsername() {
        return username;
    }

    void addSendFileMessage(SendFileMessage sendFileMessage) {
        sendFileRequests.add(sendFileMessage);
    }

    void setUsernameCorrect() {
        this.usernameCorrect = true;
    }

    void stopClient() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
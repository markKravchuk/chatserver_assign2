package com.saxion.nl.client;

import com.saxion.nl.messages.Message;
import com.saxion.nl.messages.MessageHandler;
import com.saxion.nl.messages.actual_mesages.AuthenticationMessage;
import com.saxion.nl.messages.actual_mesages.TerminateConnectionMessage;
import com.saxion.nl.messages.actual_mesages.users.GetOthersPublicKey;
import com.saxion.nl.messages.actual_mesages.users.PersonalMessage;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class MessageReceiverThread extends Thread {

    private Socket socket;
    private Key privateKey;
    private ClientLauncher parent;

    private PrintWriter writer;

    MessageReceiverThread(Socket socket, Key privateKey, ClientLauncher parent) {
        this.socket = socket;
        this.privateKey = privateKey;
        this.parent = parent;
    }

    @Override
    public void run() {
        BufferedReader reader = null;
        try {
            writer = new PrintWriter(socket.getOutputStream());
            reader = new BufferedReader(new BufferedReader(new InputStreamReader(socket.getInputStream())));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ServerResponse serverResponse = new ServerResponse(writer,parent);

        while (socket.isConnected()) {
            try {
                String line = "";
                try {
                    line = reader.readLine();
                    if (line == null) throw new SocketException();
                } catch (SocketException e) {
                    parent.stopClient();
                    System.err.println("Connection with server closed");
                    System.exit(0);
                }

                Message message = MessageHandler.getTypeMessage(line);
                if (message instanceof AuthenticationMessage) {
                    if (message.isResponse()) parent.setUsernameCorrect();  //authorization check
                } else if (message instanceof GetOthersPublicKey) {
                    String username = ((GetOthersPublicKey) message).getUsername();  //getting username of the client you want to send message to
                    String key = ((GetOthersPublicKey) message).getPublicKey();

                    PersonalMessage pm = parent.getMessageToSend(username);
                    if (pm != null) {
                        byte[] publicKeyData = Base64.getDecoder().decode(key);

                        try {
                            Cipher cipher = Cipher.getInstance("RSA");
                            X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKeyData);
                            KeyFactory kf = KeyFactory.getInstance("RSA");
                            PublicKey receiverPublicKey = kf.generatePublic(spec);   //public key generation

                            cipher.init(Cipher.ENCRYPT_MODE, receiverPublicKey);

                            String encodedMessage = Base64.getEncoder()
                                    .encodeToString(cipher.doFinal(pm.getActualMessage()
                                            .getMessage().getBytes(StandardCharsets.UTF_8)));  //encoding
                            pm.setMessage(encodedMessage);
                            sendMessage(pm);    //sending encoded message
                            parent.getMessagesToSend().remove(pm);
                        } catch (InvalidKeySpecException | NoSuchAlgorithmException | InvalidKeyException |
                                IllegalBlockSizeException | BadPaddingException | NoSuchPaddingException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (message instanceof PersonalMessage) {
                    if (message.getPrefix().startsWith("+OK")) continue;
                    String result = message.getMessage();
                    String encryptedText = ((PersonalMessage) message).getActualMessage().getMessage();
                    try {
                        Cipher cipher = Cipher.getInstance("RSA");
                        cipher.init(Cipher.DECRYPT_MODE, privateKey);
                        result = new String(cipher.doFinal(Base64.getDecoder()   //decrypting personal message
                                .decode(encryptedText)), StandardCharsets.UTF_8);
                    } catch (IllegalArgumentException | BadPaddingException
                            | NoSuchAlgorithmException | NoSuchPaddingException e) {
                        System.err.println("Oops");
                    } catch (InvalidKeyException | IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } finally {
                        System.out.println(((PersonalMessage) message).getActualMessage().getSender()
                                + " said you: " + result);
                    }
                } else if (message instanceof TerminateConnectionMessage) {
                    parent.stopClient();
                } else serverResponse.handleResponse(message, line);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMessage(Message message) {
        writer.println(message.send());
        writer.flush();
    }

}

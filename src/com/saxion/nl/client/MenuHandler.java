package com.saxion.nl.client;

import com.saxion.nl.messages.Message;
import com.saxion.nl.messages.actual_mesages.TerminateConnectionMessage;
import com.saxion.nl.messages.actual_mesages.file.SendFileMessage;
import com.saxion.nl.messages.actual_mesages.groups.*;
import com.saxion.nl.messages.actual_mesages.users.BroadcastMessage;
import com.saxion.nl.messages.actual_mesages.users.GetOthersPublicKey;
import com.saxion.nl.messages.actual_mesages.users.PersonalMessage;
import com.saxion.nl.messages.actual_mesages.users.RequestUsersMessage;
import com.saxion.nl.server.model.groups.Group;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

class MenuHandler {

    private PrintWriter writer;
    private Scanner scanner;
    private String username;
    private ClientLauncher parent;

    MenuHandler(ClientLauncher parent, PrintWriter writer, String username) {
        this.writer = writer;
        this.username = username;
        this.parent = parent;
        scanner = new Scanner(System.in);
    }

    void handleMenu() {
        System.out.println("╔═════════════════════════════════╗");
        System.out.println("║        Choose an option:        ║");
        System.out.println("╠═════════════════════════════════╣");
        System.out.println("║ 1  ═ Get list of online users   ║");
        System.out.println("║ 2  ═ Create group               ║");
        System.out.println("║ 3  ═ Join group                 ║");
        System.out.println("║ 4  ═ Send message to a group    ║");
        System.out.println("║ 5  ═ Leave group                ║");
        System.out.println("║ 6  ═ Say something to everyone  ║");
        System.out.println("║ 7  ═ Say something personal     ║");
        System.out.println("║ 8  ═ Delete group               ║");
        System.out.println("║ 9  ═ Kick user from the group   ║");
        System.out.println("║ 10 ═ Send file to another user  ║");
        System.out.println("║ 11 ═ Quit                       ║");
        System.out.println("╚═════════════════════════════════╝");

        String answer = scanner.nextLine();
        switch (answer) {
            case "1":
                getListOfOnlineUsers();
                break;
            case "2":
                createGroup();
                break;
            case "3":
                joinGroup();
                break;
            case "4":
                sendMessageToGroup();
                break;
            case "5":
                leaveGroup();
                break;
            case "6":
                sendBroadcastMessage();
                break;
            case "7":
                sendPersonalMessage();
                break;
            case "8":
                deleteGroup();
                break;
            case "9":
                kickUser();
                break;
            case "10":
                sendFile();
                break;
            case "11":
                TerminateConnectionMessage terminateConnectionMessage = new TerminateConnectionMessage();
                sendMessage(terminateConnectionMessage);
                break;
            default:
                System.out.println("Please enter the right number (1- 11)");
        }

    }

    void setUsername(String username) {
        this.username = username;
    }

    private void getListOfOnlineUsers() {
        RequestUsersMessage requestUsersMessage = new RequestUsersMessage();
        sendMessage(requestUsersMessage);
    }

    private void createGroup() {
        System.out.println("How do you want to call your group?: ");
        String message = scanner.nextLine();
        message = message.replace(" ", "_");
        CreateGroupMessage createGroupMessage = new CreateGroupMessage(message);
        sendMessage(createGroupMessage);
    }

    private void joinGroup() {
        System.out.println("List of available groups: ");
        RequestGroupsMessage requestGroupsMessage = new RequestGroupsMessage();
        sendMessage(requestGroupsMessage);
        System.out.println("Which group do you want to join?");
        String message = scanner.nextLine();
        JoinGroupMessage joinGroupMessage = new JoinGroupMessage(message);
        sendMessage(joinGroupMessage);
    }

    private void sendMessageToGroup() {
        System.out.println("Enter a group name: ");
        String groupName = scanner.nextLine();
        System.out.println("Enter a message: ");
        String message = scanner.nextLine();
        GroupMessage groupMessage = new GroupMessage(groupName, username, message);
        sendMessage(groupMessage);
    }

    private void leaveGroup() {
        System.out.println("Which group do you want to leave?");
        String message = scanner.nextLine();
        LeaveGroupMessage leaveGroupMessage = new LeaveGroupMessage(message);
        sendMessage(leaveGroupMessage);
    }

    private void sendBroadcastMessage() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What do you want to say:");
        String message = scanner.nextLine();
        BroadcastMessage broadcastMessage = new BroadcastMessage(username, message);
        sendMessage(broadcastMessage);
    }

    /**
     * Option #7 from menu
     */
    private void sendPersonalMessage() {
        String message, receiver;
        Scanner scanner = new Scanner(System.in);

        System.out.println("What do you want to say?");
        message = scanner.nextLine();
        System.out.println("Who do you want to talk to?");
        receiver = scanner.nextLine();

        //add to queue to send message when public key will be received
        PersonalMessage personalMessage = new PersonalMessage(message, receiver);
        parent.addMessageToSend(personalMessage);

        //get receiver`s public key
        GetOthersPublicKey getOthersPublicKey = new GetOthersPublicKey(receiver);
        sendMessage(getOthersPublicKey);
    }

    /**
     * Option 8 from menu
     */
    private void deleteGroup() {
        String message;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which group do you want to delete?");
        message = scanner.nextLine();
        RemoveGroupMessage removeGroupMessage = new RemoveGroupMessage(message);
        sendMessage(removeGroupMessage);
    }

    /**
     * Option 9 from menu
     */
    private void kickUser() {
        String group, user;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which group you want to kick user from?");
        group = scanner.nextLine();
        System.out.println("Enter a user to remove from group " + group);
        user = scanner.nextLine();
        sendMessage(new KickUserMessage(group, user));
    }

    /**
     * Option 10 from menu
     */
    private void sendFile() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the file path: (skip to send default) ");
        String filepath = scanner.nextLine();

        boolean filenameCorrect = false;

        File myFile = null;
        byte[] encodedHash = new byte[0];

        do {
            try {
                myFile = new File(filepath);
                FileInputStream is = new FileInputStream(myFile);
                // by opening a file input stream system verifies that file exists
                // and reads all the data from file to hash it

                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                encodedHash = digest.digest(is.readAllBytes());

                is.close();
                filenameCorrect = true;
            } catch (NoSuchAlgorithmException | IOException e) {
                System.out.println("File with such path does not exists, try again");
                filepath = scanner.nextLine();
            }
        } while (!filenameCorrect);

        System.out.println("Choose a user to send: ");
        String receiver = scanner.nextLine();

        //make the hashed bytes to String
        String hashCode = FileProceed.hashedCode(encodedHash);

        SendFileMessage sendFileMessage = new SendFileMessage
                (username, receiver, myFile.getName(), myFile.length(), hashCode);

        parent.addFile(sendFileMessage.send(),myFile);

        sendMessage(sendFileMessage);
    }

    private void sendMessage(Message message) {
        writer.println(message.send());
        writer.flush();
    }



}

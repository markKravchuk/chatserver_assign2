package com.saxion.nl.server.model.data_providers;

import java.util.ArrayList;
import java.util.HashMap;

public class UserDataProvider {

    private ArrayList<String> usernames;
    private HashMap<String, String> userKeys;
    private static UserDataProvider instance;

    public UserDataProvider() {
        usernames = new ArrayList<>();
        userKeys = new HashMap<>();
    }

    static {
        instance = new UserDataProvider();
    }

    public boolean addUsername(String name) {
        if(!usernames.contains(name)) {
            usernames.add(name);
        } else return false;
        return true;
    }

    public void putUserKey(String key, String username) {
        userKeys.put(username,key);
    }

    public String getUserKey(String username) {
        return userKeys.get(username);
    }

    public boolean addUsernameFileTransfer(String name){
        if(!usernames.contains(name)) {
            usernames.add(name);
        } else return false;
        return true;
    }



    public static UserDataProvider getInstance() {
        return instance;
    }

    public String getAllUsers() {
        String result = "";

        for(String user : usernames) {
            result += user + ' ';
        }

        //last spacebar is not needed

        int lastIndex = result.length()-1;
        if(lastIndex == -1) {
            lastIndex = 0;
        }
        return result.substring(0,lastIndex);
    }

    public void removeUser(String username) {
        usernames.remove(username);
        userKeys.remove(username);

        GroupDataProvider groupDataProvider = GroupDataProvider.getInstance();
        groupDataProvider.removeUser(username);
    }

}

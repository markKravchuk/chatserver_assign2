package com.saxion.nl.server.model.data_providers;

import com.saxion.nl.server.model.groups.Group;

import java.util.ArrayList;

public class GroupDataProvider {

    private static GroupDataProvider instance;
    private ArrayList<Group> groups;

    static {
        instance = new GroupDataProvider();
    }

    public GroupDataProvider(){
        groups = new ArrayList<>();
    }

    public boolean addGroup(String name, String creator) {

        boolean exists = false;

        for(Group group : groups) {

            if(group.getName().equals(name)) {
                exists = true;
                break;
            }
        }
        if(exists) return false;

        Group group = new Group(name, creator);
        groups.add(group);

        return true;
    }

    public void printGroups(){
        for(Group group : groups) {
            System.out.println("Group name: " + group.getName() + ", people in: " + group.getUsers().size());
        }
    }

    public ArrayList<String> getUsersOfGroup(String groupName, String user) throws Exception {
        for(Group group : groups) {
            if(group.getName().equals(groupName)) {
                if(group.containsUser(user)) return new ArrayList<>(group.getUsers());
                else throw new Exception("User is not in a group!");
            }
        }
        return null;
    }

    public void removeUser(String username) {
        for(Group group : groups) {
            group.removeUser(username);
        }
    }

    public String removeGroup(String groupName, String username) {
        for(Group group : groups) {
            if(group.getName().equals(groupName)) {
                if(group.getCreator().equals(username)) {
                    if(groups.remove(group)) {
                        return null;
                    } else {
                        return "something went wrong while removing the group";
                    }
                } else {
                    return "only creator of group can remove it";
                }
            }
        }
        return "no such group";
    }

    /**
     * Returning null if everything went fine
     */

    public String kickFromGroup(String groupName, String username, String userToKick) {
        for(Group group : groups) {
            if(group.getName().equals(groupName)) {
                if(group.getCreator().equals(username)) {
                    if(group.removeUser(userToKick)) {
                        return null;
                    } else {
                        return "such user is not in the group";
                    }
                } else {
                    return "only creator of group can remove it";
                }
            }
        }
        return "no such group";
    }

    public String leaveGroup(String groupName, String username) {
        for(Group group : groups) {
            if(group.getName().equals(groupName)) {
                if(group.removeUser(username)) {
                    return null;
                } else {
                    return "User is not a member of that group";
                }
            }
        }
        return "Group not found :(";
    }

    public String getGroupNames() {
        String result = "";

        for (int i = 0; i < groups.size(); i++) {
            if(i != 0) {
                result += ' ';
            }

            result += groups.get(i).getName();
        }

        return result;
    }

    public static GroupDataProvider getInstance() {
        return instance;
    }

    public String joinGroup(String groupName, String user) {

        boolean groupExists = false;

        for(Group group : groups) {
            if(group.getName().equals(groupName)) {
                groupExists = true;
                if(!group.addUser(user)) {
                    return "User already exists in group " + group.getName();
                }
            }
        }
        //returning null if everything fine
        if(groupExists) {
            return null;
        } else {
            return "Group not found";
        }
    }

}

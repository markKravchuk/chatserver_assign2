package com.saxion.nl.server.model.file_server;

public class FileDataSlices {

    private byte[] byteArray;
    private long bytesRead;
    private String filename;

    public FileDataSlices(byte[] byteArray, long bytesRead, String filename) {
        this.byteArray = byteArray;
        this.bytesRead = bytesRead;
        this.filename = filename;
    }

    public byte[] getByteArray() {
        return byteArray;
    }

    public String getFilename() {
        return filename;
    }

    public long getBytesRead() {
        return bytesRead;
    }
}

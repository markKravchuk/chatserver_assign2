package com.saxion.nl.server.model.groups;

import com.saxion.nl.server.model.UserMessage;

import java.util.ArrayList;

public class Group {

    private String name;
    private String creator;
    private ArrayList<String> users;

    public Group(String name, String creator) {
        this.name = name;
        this.creator = creator;
        this.users  = new ArrayList<>();
        //creator is also a user of group
        users.add(creator);
    }

    public boolean addUser(String user) {
        if(users.contains(user)) {
            return false;
        }
        users.add(user);
        return true;
    }

    public boolean containsUser(String username) {
        return users.contains(username);
    }

    public boolean removeUser(String user) {
        if(users.contains(user)) {
            users.remove(user);
            return true;
        }
        return false;
    }

    public String getCreator() {
        return creator;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getUsers() {
        return users;
    }
}

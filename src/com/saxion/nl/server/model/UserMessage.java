package com.saxion.nl.server.model;

public class UserMessage {

    private String sender;
    private String receiver;
    private String message;

    public UserMessage(String message, String receiver) {
        this.receiver = receiver;
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getSender() {
        return sender;
    }
}

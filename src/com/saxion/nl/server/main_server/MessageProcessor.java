package com.saxion.nl.server.main_server;

import com.saxion.nl.messages.Message;
import com.saxion.nl.messages.actual_mesages.AuthenticationMessage;
import com.saxion.nl.messages.actual_mesages.TerminateConnectionMessage;
import com.saxion.nl.messages.actual_mesages.errors.BaseErrorMessage;
import com.saxion.nl.messages.actual_mesages.file.SendFileMessage;
import com.saxion.nl.messages.actual_mesages.groups.*;
import com.saxion.nl.messages.actual_mesages.ping_pong.PongMessage;
import com.saxion.nl.messages.actual_mesages.users.*;
import com.saxion.nl.server.model.data_providers.GroupDataProvider;
import com.saxion.nl.server.model.data_providers.UserDataProvider;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class MessageProcessor {

    private PrintWriter writer;
    private String username;
    private PingSenderThread pinger;
    private ClientThread clientConnection;

    public MessageProcessor(ClientThread parent) {
        this.clientConnection = parent;
        this.writer = parent.getWriter();
        this.username = parent.getUsername();
        this.pinger = parent.getPingSender();
    }

    void processMessage(Message message) {

        if(message instanceof BaseErrorMessage) {
            sendMessage(message);
            return;
        }

        if (message instanceof AuthenticationMessage) {

            if (username != null) {
                sendMessage(new BaseErrorMessage("user already logged in"));
                return;
            }

            UserDataProvider dataProvider = UserDataProvider.getInstance();

            if(!usernameIsFine(message.getMessage())) {
                sendMessage(new BaseErrorMessage("Username contains invalid characters"));
                return;
            }

            if (dataProvider.addUsername(message.getMessage())) {
                message.setResponse(true);
                username = message.getMessage();
                clientConnection.setUsername(username);
                sendMessage(message);
                return;
            } else {
                sendMessage(new BaseErrorMessage("user already exists"));
                return;
            }
        }

        if (message instanceof PongMessage) {
            //indicate the thread that client responded
            pinger.setReceivedPong(true);
            return;
        }

        if (message instanceof TerminateConnectionMessage) {
            message.setResponse(true);
            sendMessage(message);
            pinger.stopSending();
            clientConnection.terminateConnection();
        }

        //also to terminate connection is accessible with no log in option

        //if user is unauthorized -> no access to other message

        if (username == null) {
            sendMessage(new BaseErrorMessage("user needs to be logged in first"));
            return;
        }

        if (message instanceof RequestUsersMessage) {

            message.setResponse(true);

            sendMessage(message);

        }

        if (message instanceof CreateGroupMessage) {
            //create group
            GroupDataProvider groupDataProvider = GroupDataProvider.getInstance();

            String groupName = message.getMessage();

            if (groupName == null) {
                sendMessage(new BaseErrorMessage("include a group name"));
                return;
            }
            //returned true if group name is unique
            if (groupDataProvider.addGroup(groupName, username)) {
                message.setResponse(true);
                groupDataProvider.printGroups();
                sendMessage(message);
            } else {
                sendMessage(new BaseErrorMessage("group already exists"));
            }
            return;
        }

        if (message instanceof JoinGroupMessage) {
            String groupToJoin = message.getMessage();

            GroupDataProvider groupDataProvider = GroupDataProvider.getInstance();

            String result = groupDataProvider.joinGroup(groupToJoin, username);

            if (result != null) {
                sendMessage(new BaseErrorMessage(result));
                return;
            }

            message.setResponse(true);

            sendMessage(message);
            return;
        }

        if (message instanceof RequestGroupsMessage) {
            GroupDataProvider groupProvider = GroupDataProvider.getInstance();

            String groups = groupProvider.getGroupNames();

            ((RequestGroupsMessage) message).fillGroups(groups);

            message.setResponse(true);

            sendMessage(message);
            return;
        }

        if (message instanceof LeaveGroupMessage) {
            GroupDataProvider dataProvider = GroupDataProvider.getInstance();

            String groupToLeave = message.getMessage();

            String result = dataProvider.leaveGroup(groupToLeave, username);

            if (result != null) {
                sendMessage(new BaseErrorMessage(result));
                return;
            } else {
                message.setResponse(true);
                sendMessage(message);
                return;
            }
        }

        if (message instanceof BroadcastMessage) {
            BroadcastMessage toReturn = new BroadcastMessage((BroadcastMessage) message);

            ((BroadcastMessage) message).setUsername(username);
            clientConnection.getParent().broadcastSomething((BroadcastMessage) message);

            toReturn.setResponse(true);

            sendMessage(toReturn);
        }

        if (message instanceof PersonalMessage) {

            ((PersonalMessage) message).setSender(username);

            PersonalMessage toSend = new PersonalMessage((PersonalMessage) message);
            toSend.setResponse(true);
            sendMessage(toSend);

            ((PersonalMessage) message).isForReceiver(true);
            ((PersonalMessage) message).setSender(username);

            clientConnection.getParent().personalMessage((PersonalMessage) message);
        }

        if (message instanceof GroupMessage) {
            ((GroupMessage) message).setSender(username);

            GroupMessage toSend = new GroupMessage((GroupMessage) message);

            try {
                ArrayList<String> usersOfGroup = GroupDataProvider.getInstance()
                        .getUsersOfGroup(((GroupMessage) message).getGroupName(), username);

                if (usersOfGroup == null) {
                    sendMessage(new BaseErrorMessage("group does not exists"));
                    return;
                }

                toSend.setResponse(true);
                sendMessage(toSend);

                ((GroupMessage) message).isForReceiver(true);

                clientConnection.getParent().groupMessage((GroupMessage) message, usersOfGroup);
            } catch (Exception e) {
                sendMessage(new BaseErrorMessage(e.getMessage()));
            }
            return;
        }

        if (message instanceof RemoveGroupMessage) {
            String groupToRemove = message.getMessage();

            GroupDataProvider dataProvider = GroupDataProvider.getInstance();

            String result = dataProvider.removeGroup(groupToRemove,username);

            if(result == null) {
                message.setResponse(true);
                sendMessage(message);
                return;
            } else {
                sendMessage(new BaseErrorMessage(result));
            }

        }

        if (message instanceof KickUserMessage) {
            GroupDataProvider provider = GroupDataProvider.getInstance();

            String result = provider.kickFromGroup(((KickUserMessage) message).getGroup(),username,
                    ((KickUserMessage) message).getUserToKick());

            if(result == null) {
                message.setResponse(true);
                sendMessage(message);
            } else {
                sendMessage(new BaseErrorMessage(result));
            }
        }

        if (message instanceof SendFileMessage) {
            if(!clientConnection.getParent().sendFileMessage((SendFileMessage) message)) {
                sendMessage(new BaseErrorMessage("Such user does not exists"));
            };
        }

        if (message instanceof PublicKeyMessage) {
            UserDataProvider.getInstance().putUserKey(((PublicKeyMessage) message).getKey(), username);

            message.setResponse(true);

            sendMessage(message);
        }

        if (message instanceof GetOthersPublicKey) {
            String userKey = UserDataProvider.getInstance().getUserKey(((GetOthersPublicKey) message).getUsername());

            ((GetOthersPublicKey) message).setPublicKey(userKey);

            message.setResponse(true);

            sendMessage(message);
        }
    }

    private boolean usernameIsFine(String username) {
        return true;
    }

    private void sendMessage(Message message) {
        System.out.println("[" + username + "] >> " + message.send());
        writer.println(message.send());
        writer.flush();
    }

}

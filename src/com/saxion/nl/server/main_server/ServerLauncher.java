package com.saxion.nl.server.main_server;

import com.saxion.nl.messages.actual_mesages.file.SendFileMessage;
import com.saxion.nl.messages.actual_mesages.groups.GroupMessage;
import com.saxion.nl.messages.actual_mesages.users.BroadcastMessage;
import com.saxion.nl.messages.actual_mesages.users.PersonalMessage;
import com.saxion.nl.server.file_server.FileServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ServerLauncher {

    private static final int PORT = 1500;

    private ArrayList<ClientThread> userConnections;

    private FileServer fileServer;

    public static void main(String[] args) throws IOException {
        new ServerLauncher().run();
    }

    void broadcastSomething(BroadcastMessage message) {
        for(ClientThread connection : userConnections) {
            connection.receiveMessage(message);
        }
    }

    void personalMessage(PersonalMessage message) {
        for(ClientThread connection : userConnections) {
            if(connection.getUsername().equals(message.getReceiver()) ) {
                connection.receiveMessage(message);
                return;
            }
        }
        System.err.println("No such user: " + message.getReceiver());
    }

    void groupMessage(GroupMessage message, ArrayList<String> usersInvolved) {
        for(ClientThread thread : userConnections) {
            for (int i = 0; i < usersInvolved.size(); i++) {
                if(thread.getUsername().equals(usersInvolved.get(i)) &&
                        !thread.getUsername().equals(message.getSender())) {
                    thread.receiveMessage(message);
                    usersInvolved.remove(i);
                    i--;
                }
            }
        }
    }

    boolean sendFileMessage(SendFileMessage sendFileMessage) {
        for(ClientThread client : userConnections) {
            if(client.getUsername() == null) continue;
            if(client.getUsername().equals(sendFileMessage.getReceiver())) {
                client.receiveMessage(sendFileMessage);
                return true;
            }
        }
        return false;
    }

    private void run() throws IOException {
        System.out.println("Hello, this is server");
        userConnections = new ArrayList<>();

        ServerSocket serverSocket = new ServerSocket(PORT);

        fileServer = new FileServer();
        fileServer.start();

        while(true) {
            Socket socket = serverSocket.accept();

            ClientThread clientConnection = new ClientThread(socket,this);
            clientConnection.start();
            userConnections.add(clientConnection);

            System.out.println("Server accepter a socket");
        }
    }

    boolean killThread(ClientThread thread) {
        return userConnections.remove(thread);
    }
}

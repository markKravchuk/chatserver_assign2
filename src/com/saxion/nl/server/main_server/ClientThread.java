package com.saxion.nl.server.main_server;

import com.saxion.nl.messages.Message;
import com.saxion.nl.messages.MessageHandler;
import com.saxion.nl.messages.actual_mesages.*;
import com.saxion.nl.messages.actual_mesages.file.SendFileMessage;
import com.saxion.nl.messages.actual_mesages.groups.*;
import com.saxion.nl.messages.actual_mesages.users.BroadcastMessage;
import com.saxion.nl.messages.actual_mesages.users.PersonalMessage;
import com.saxion.nl.server.model.data_providers.UserDataProvider;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.rmi.ServerException;

public class ClientThread extends Thread {

    private Socket socket;
    private InputStream inputStream;
    private OutputStream outputStream;
    private BufferedReader reader;
    private PrintWriter writer;

    private ServerLauncher parent;

    private String username;

    private PingSenderThread pingSender;

    private MessageProcessor processor;

    public ClientThread(Socket socket, ServerLauncher parent) {
        this.socket = socket;
        this.parent = parent;
        try {
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            writer = new PrintWriter(outputStream);
            reader = new BufferedReader(new InputStreamReader(inputStream));

            pingSender = new PingSenderThread(this);
            processor = new MessageProcessor(this);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        pingSender.start();
        //send a welcome message to the client
        sendMessage(new WelcomeMessage());
        while (socket.isConnected()) {
            try {
                String receivedLine;

                try {
                    receivedLine = reader.readLine();
                    System.out.println("[" + username + "] << " + receivedLine);
                } catch (ServerException | SocketException e) {
                    break;
                }

                Message message = MessageHandler.getTypeMessage(receivedLine);

                processor.processMessage(message);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void terminateConnection() {
        try {
            pingSender = null;
            socket.close();
            UserDataProvider users = UserDataProvider.getInstance();
            users.removeUser(username);
            System.out.println("Socket with " + username + " is closed now");
            parent.killThread(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void receiveMessage(Message message) {
        if(message instanceof BroadcastMessage) {
            if (!((BroadcastMessage) message).getUsername().equals(username)) {
                sendMessage(message);
            }
        } else {
            sendMessage(message);
        }
    }

    PrintWriter getWriter() {
        return writer;
    }

    PingSenderThread getPingSender() {
        return pingSender;
    }

    String getUsername() {
        return username;
    }

    public ServerLauncher getParent() {
        return parent;
    }

    void setUsername(String username) {
        this.username = username;
    }

    void sendMessage(Message message) {
        System.out.println("[" + username + "] >> " + message.send());
        writer.println(message.send());
        writer.flush();
    }
}

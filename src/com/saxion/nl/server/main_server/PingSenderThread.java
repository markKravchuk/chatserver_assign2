package com.saxion.nl.server.main_server;

import com.saxion.nl.messages.actual_mesages.ping_pong.PingMessage;
import com.saxion.nl.messages.actual_mesages.ping_pong.PongTimeoutMessage;
import com.saxion.nl.server.main_server.ClientThread;

import java.util.Timer;
import java.util.TimerTask;

public class PingSenderThread extends Thread {

    private ClientThread parent;
    private boolean receivedPong;
    private final boolean[] keepSending = {true};

    private static final int DISTANCE_IN_PING = 5000;

    public PingSenderThread(ClientThread parent) {
        this.parent = parent;
    }

    @Override
    public void run() {

        while (keepSending[0]) {
            try {
                Thread.sleep(DISTANCE_IN_PING / 10);

                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        if (!receivedPong) {
                            //close connection
                            parent.sendMessage(new PongTimeoutMessage());
                            keepSending[0] = false;
                            parent.terminateConnection();
                        }
                    }
                };

                receivedPong = false;

                Timer timer = new Timer("task");

                parent.sendMessage(new PingMessage());

                timer.schedule(task, (int) (DISTANCE_IN_PING * 0.8));

                Thread.sleep(DISTANCE_IN_PING);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        parent.terminateConnection();

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void stopSending() {
        keepSending[0] = false;
        receivedPong = true;
    }

    public void setReceivedPong(boolean receivedPong) {
        this.receivedPong = receivedPong;
    }
}

package com.saxion.nl.server.file_server;

import com.saxion.nl.messages.Message;
import com.saxion.nl.messages.MessageHandler;
import com.saxion.nl.messages.actual_mesages.AuthenticationMessage;
import com.saxion.nl.messages.actual_mesages.errors.BaseErrorMessage;
import com.saxion.nl.messages.actual_mesages.file.SendFileMessage;
import com.saxion.nl.messages.actual_mesages.file.SendFileResponse;
import com.saxion.nl.server.model.file_server.FileDataSlices;

import java.io.*;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ClientFileThread extends Thread {

    private Socket socket;
    private InputStream inputStream;
    private OutputStream outputStream;
    private BufferedReader reader;
    private PrintWriter writer;

    private FileServer parent;

    private String username;

    ClientFileThread(Socket socket, FileServer parent) {
        this.socket = socket;
        this.parent = parent;
        try {
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            writer = new PrintWriter(outputStream);
            reader = new BufferedReader(new InputStreamReader(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            String line = reader.readLine();
            Message message = MessageHandler.getTypeMessage(line);

            boolean authenticated = false;

            if (message instanceof AuthenticationMessage) authenticated = true;

            while (!authenticated) {
                writer.println(new BaseErrorMessage("Please start with authentication").send());
                writer.flush();
                line = reader.readLine();
                message = MessageHandler.getTypeMessage(line);
                if (message instanceof AuthenticationMessage) authenticated = true;
            }

            message.setResponse(true);
            writer.println(message.send());
            writer.flush();

            username = message.getMessage();

            //receive file info
            String messageStr = reader.readLine();
            SendFileMessage sendFileMessage = (SendFileMessage) MessageHandler.getTypeMessage(messageStr);

            if (!sendFileMessage.getPrefix().startsWith("+OK")) {

                long fileLength = sendFileMessage.getSize();

                int bytesRead;
                int current = 0;

                byte[] myBytArray = new byte[(int) fileLength];

                boolean receivedCorrect = false;

                do {
                    inputStream.read(myBytArray, 0, myBytArray.length);

                    byte[] encodedHash = new byte[0];
                    try {
                        MessageDigest digest = MessageDigest.getInstance("SHA-256");
                        encodedHash = digest.digest(myBytArray);
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }

                    StringBuilder hexString = new StringBuilder();
                    for (byte hash : encodedHash) {
                        String hex = Integer.toHexString(0xff & hash);
                        if (hex.length() == 1) hexString.append('0');
                        hexString.append(hex);
                    }

                    String hashCode = hexString.toString();

                    if (sendFileMessage.getChecksum().equals(hashCode)) {
                        receivedCorrect = true;
                        writer.println(new SendFileResponse(true).send());
                        writer.flush();
                    } else {
                        System.out.println("[fs] [" + username + "] checksum of received file is not correct," +
                                " waiting to receive again");
                        writer.println(new SendFileResponse(false).send());
                        writer.flush();
                    }

                } while(!receivedCorrect);

                parent.sendFileToClient(myBytArray, sendFileMessage.getReceiver());
            } else {
                System.out.println("[fs] [" + username + "] is waiting...");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    void sendFileToClient(byte[] fileBytes) {
        try {
            boolean receivedCorrectly = false;

            do {
                outputStream.write(fileBytes, 0, fileBytes.length);
                System.out.println("[fs] [" + username + "] >> sending file");
                outputStream.flush();

                String resultStr = reader.readLine();
                Message result = MessageHandler.getTypeMessage(resultStr);
                if(result instanceof SendFileResponse) {
                    if(((SendFileResponse) result).isCorrect()) {
                        receivedCorrectly = true;
                    }
                } else if (result instanceof BaseErrorMessage) {
                    System.out.println("[fs] [" + username + "] Error: " + result.getMessage());
                }
            } while (!receivedCorrectly);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }
}
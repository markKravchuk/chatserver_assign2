package com.saxion.nl.server.file_server;

import com.saxion.nl.server.main_server.ClientThread;
import com.saxion.nl.server.model.file_server.FileDataSlices;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class FileServer extends Thread{

    private static final int PORT = 1338;

    private ArrayList<ClientFileThread> userConnections;

    public void run() {
        System.out.println("File transfer server is alive");
        userConnections = new ArrayList<>();
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);

            while (true) {
                Socket socket = serverSocket.accept();

                ClientFileThread clientConnection = new ClientFileThread(socket, this);
                clientConnection.start();
                userConnections.add(clientConnection);

                System.out.println("[fs] Client [" + clientConnection.getUsername() + "] connected to fileserver");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void sendFileToClient(byte[] fileBytes, String receiver) {
        for(ClientFileThread client : userConnections) {
            if(client.getUsername().equals(receiver)) {
                client.sendFileToClient(fileBytes);
                return;
            }
        }
    }

    public boolean killThread(ClientThread thread) {
        return userConnections.remove(thread);
    }

}

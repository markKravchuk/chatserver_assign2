package com.saxion.nl.messages;

import com.saxion.nl.messages.actual_mesages.*;
import com.saxion.nl.messages.actual_mesages.errors.BaseErrorMessage;
import com.saxion.nl.messages.actual_mesages.file.SendFileMessage;
import com.saxion.nl.messages.actual_mesages.file.SendFileResponse;
import com.saxion.nl.messages.actual_mesages.groups.*;
import com.saxion.nl.messages.actual_mesages.ping_pong.PingMessage;
import com.saxion.nl.messages.actual_mesages.ping_pong.PongMessage;
import com.saxion.nl.messages.actual_mesages.users.*;

public class MessageHandler {

    public static Message getTypeMessage(String line) {

        if(line == null) return new BaseErrorMessage("Nothing was provided to instantiate");

        if(line.equals("HELO Welcome to ChatServer!")) {
            return new WelcomeMessage();
        }

        if (line.startsWith("HELO")) {
            if (line.length() < 6) {
                return new BaseErrorMessage("Not enough arguments provided");
            }
            return new AuthenticationMessage(line.substring(5));
        }

        if (line.startsWith("GET")) {
            //there are several types of get messages

            if (line.startsWith("GET ONLINE_USERS")) {
                return new RequestUsersMessage();
            }

            if (line.startsWith("GET GROUPS")) {

                String[] arguments = line.split(" ");
                if(arguments.length < 2) return new BaseErrorMessage("Not enough arguments");

                StringBuilder groups = new StringBuilder();

                for(int i = 2; i < arguments.length; i++) {
                    groups.append(arguments[i]);
                    if (i != arguments.length - 1) groups.append(" ");
                }

                return new RequestGroupsMessage(groups.toString());
            }

            if (line.startsWith("GET PUBLIC KEY")) {
                String[] attributes = line.split(" ");
                if (attributes.length < 4) return new BaseErrorMessage("Not enough arguments");

                if(attributes.length == 4) {
                    return new GetOthersPublicKey(attributes[3]);
                } else {
                    return new GetOthersPublicKey(attributes[3], attributes[4]);
                }
            }
        }

        if (line.startsWith("PING")) {
            return new PingMessage();
        }

        if (line.startsWith("PONG")) {
            return new PongMessage();
        }

        if (line.startsWith("CREATE GROUP")) {
            if (line.length() < 14) {
                return new BaseErrorMessage("Not enough arguments provided");
            }
            return new CreateGroupMessage(line.substring(13));
        }

        if (line.startsWith("JOIN GROUP")) {
            if (line.length() < 12) {
                return new BaseErrorMessage("Not enough arguments provided");
            }
            return new JoinGroupMessage(line.substring(11));
        }

        if (line.startsWith("LEAVE GROUP")) {
            if (line.length() < 13) {
                return new BaseErrorMessage("Not enough arguments provided");
            }
            return new LeaveGroupMessage(line.substring(12));
        }

        if (line.startsWith("BCST ")) {

            String[] arguments = line.split(" ");

            //BCST _username_ _message_ _message(2)_
            if (arguments.length < 2) {
                return new BaseErrorMessage("Not enough arguments provided");
            }

//            String sender = arguments[1];
            StringBuilder message = new StringBuilder();

            for(int i = 1; i < arguments.length;i++) {
                message.append(arguments[i]);
                if(i != arguments.length -1) message.append(" ");
            }

            return new BroadcastMessage(message.toString());
        }

        if (line.startsWith("SEND FILE RESPONSE")) {
            if (line.length() < 19) return new BaseErrorMessage("Not enough arguments provided");

            String response = line.substring(19);
            if(response.startsWith("true")) {
                return new SendFileResponse(true);
            } else if (response.startsWith("false")) {
                return new SendFileResponse(false);
            } else return new BaseErrorMessage("The information was not recognised");
        }

        if (line.contains("SEND FILE")) {

            String[] messageParts = line.split(" ");

            if (messageParts.length < 7) return new BaseErrorMessage("Not enough arguments applied");

            int startingIndex = 0;

            Boolean isPositiveResponse = null;

            if(line.startsWith("+OK")) {
                startingIndex++;
                isPositiveResponse = true;
            } else if (line.startsWith("-OK")) {
                startingIndex++;
                isPositiveResponse = false;
            }

            String sender = messageParts[2 + startingIndex];
            String receiver = messageParts[3 + startingIndex];
            String filename = messageParts[4 + startingIndex];
            long size = Long.parseLong(messageParts[5 + startingIndex]);
            String hashCode = messageParts[6 + startingIndex];

            SendFileMessage message = new SendFileMessage(sender,receiver,filename,size, hashCode);

            if(isPositiveResponse != null) {
                if(isPositiveResponse) {
                    message.setResponse(true);
                } else {
                    message.rejectFile();
                }
            }

            return message;
        }

        if (line.startsWith("MESSAGE GROUP") || line.startsWith("SEND GROUP")) {
                String[] arguments = line.split(" ");

                if(arguments.length < 5) return new BaseErrorMessage("Not enough of arguments");

                String group = arguments[2];
                String sender = arguments[3];

                StringBuilder sb = new StringBuilder();

                for (int i = 4; i < arguments.length; i++) {
                    sb.append(arguments[i]);
                    if(i != arguments.length - 1) sb.append(" ");
                }

                return new GroupMessage(group,sender,sb.toString());
            }

        if (line.startsWith("SEND ")) {
            //user can send to the group and direct
            String[] messageParts = line.split(" ");

            if (messageParts.length < 3) {
                return new BaseErrorMessage("Not enough arguments");
            }

//            if () {
//
//                if(messageParts.length < 5) return new BaseErrorMessage("Not enough arguments");
//
//                String groupName = messageParts[2];
//                String sender = messageParts[3];
//                StringBuilder message = new StringBuilder();
//                for (int i = 4; i < messageParts.length; i++) {
//                    message.append(messageParts[i]);
//                    if(i != messageParts.length - 1) message.append(" ");
//                }
//                return new GroupMessage(groupName, sender, message.toString());
//            }

            String receiver = messageParts[1];
            StringBuilder message = new StringBuilder();

            for (int i = 2; i < messageParts.length; i++) {
                if (i != 2) message.append(' ');
                message.append(messageParts[i]);
            }
            return new PersonalMessage(message.toString(), receiver);
        }

        if (line.startsWith("MESSAGE")) {
            String[] arguments = line.split(" ");

            if(arguments.length < 2) return new BaseErrorMessage("Not enough arguments");

            String sender = arguments[1];
            String message = arguments[2];

            PersonalMessage pm = new PersonalMessage(message,sender);
            pm.getActualMessage().setSender(sender);
            return pm;
        }

        if (line.startsWith("REMOVE GROUP")) {
            if (line.length() < 14) {
                return new BaseErrorMessage("Not enough arguments provided");
            }
            String groupToRemove = line.substring(13);
            return new RemoveGroupMessage(groupToRemove);
        }

        if (line.startsWith("KICK ")) {

            String[] messageParts = line.split(" ");

            if (messageParts.length != 3) {
                return new BaseErrorMessage("Amount of arguments do not match requirements");
            }

            String group = messageParts[1];
            String user = messageParts[2];

            return new KickUserMessage(group, user);

        }

        if (line.startsWith("QUIT")) {
            return new TerminateConnectionMessage();
        }

        if (line.startsWith("+OK Goodbye")) {
            TerminateConnectionMessage message = new TerminateConnectionMessage();
            message.setResponse(true);
            return message;
        }

        if(line.startsWith("PUBLIC KEY")) {
            if (line.length() < 11) return new BaseErrorMessage("Public key was not introduced");
            return new PublicKeyMessage(line.substring(11));
        }

        if (line.startsWith("+OK ")) {
            Message message = getTypeMessage(line.substring(4));
            message.setResponse(true);
            return message;
        }

        //if received something unknown, later on replace with ERR
        return new BaseErrorMessage("Wrong message type");
    }


}

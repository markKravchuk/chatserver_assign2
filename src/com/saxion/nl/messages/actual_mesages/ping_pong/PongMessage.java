package com.saxion.nl.messages.actual_mesages.ping_pong;

import com.saxion.nl.messages.Message;

public class PongMessage extends Message {

    public PongMessage() {
        super("PONG","");
    }


    @Override
    public String send() {
        return getPrefix();
    }
}

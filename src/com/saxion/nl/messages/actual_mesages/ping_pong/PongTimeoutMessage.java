package com.saxion.nl.messages.actual_mesages.ping_pong;

import com.saxion.nl.messages.Message;

public class PongTimeoutMessage extends Message {

    public PongTimeoutMessage() {
        super("DSCN Pong timeout","");
    }

    @Override
    public String send() {
        return getPrefix();
    }
}

package com.saxion.nl.messages.actual_mesages.ping_pong;

import com.saxion.nl.messages.Message;

public class PingMessage extends Message {

    public PingMessage() {
        super("PING","");
    }

    @Override
    public String send() {
        return getPrefix();
    }
}

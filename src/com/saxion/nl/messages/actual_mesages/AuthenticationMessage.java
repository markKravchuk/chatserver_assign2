package com.saxion.nl.messages.actual_mesages;

import com.saxion.nl.messages.Message;

public class AuthenticationMessage extends Message {

    public AuthenticationMessage(String username) {
        super("HELO",username);
    }

    @Override
    public String send() {
        return getPrefix() + ' ' + getMessage();
    }
}

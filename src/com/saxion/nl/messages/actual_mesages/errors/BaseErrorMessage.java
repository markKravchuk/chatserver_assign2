package com.saxion.nl.messages.actual_mesages.errors;

import com.saxion.nl.messages.Message;

public class BaseErrorMessage extends Message {

    public BaseErrorMessage(String error) {
        super("-ERR", error);
    }

    @Override
    public String send() {
        return getPrefix() + ' ' + getMessage();
    }
}

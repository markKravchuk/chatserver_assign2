package com.saxion.nl.messages.actual_mesages;

import com.saxion.nl.messages.Message;

public class TerminateConnectionMessage extends Message {

    public TerminateConnectionMessage() {
        super("QUIT", "");
    }

    @Override
    public void setResponse(boolean response) {
        setPrefix("+OK Goodbye");
    }

    @Override
    public String send() {
        return getPrefix();
    }
}

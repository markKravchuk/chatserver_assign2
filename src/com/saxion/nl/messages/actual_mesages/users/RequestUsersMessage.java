package com.saxion.nl.messages.actual_mesages.users;

import com.saxion.nl.messages.Message;
import com.saxion.nl.server.model.data_providers.UserDataProvider;

public class RequestUsersMessage extends Message {

    public RequestUsersMessage() {
        super("GET","ONLINE_USERS");
    }

    @Override
    public String send() {
        //get users from arraylist
        return getPrefix() + ' ' + getMessage() + ' ' + UserDataProvider.getInstance().getAllUsers();
    }
}

package com.saxion.nl.messages.actual_mesages.users;

import com.saxion.nl.messages.Message;
import com.saxion.nl.server.model.UserMessage;

public class PersonalMessage extends Message {

    private UserMessage actualMessage;

    public PersonalMessage(String message, String receiver) {
        super("SEND","");
        actualMessage = new UserMessage(message,receiver);
    }

    //copy constructor
    public PersonalMessage(PersonalMessage message) {
        super(message.getPrefix(),"");
        actualMessage = message.getActualMessage();
    }

    public UserMessage getActualMessage() {
        return actualMessage;
    }

    public void setSender(String sender) {
        actualMessage.setSender(sender);
    }

    public void setMessage(String message) {
        actualMessage.setMessage(message);
    }

    public void isForReceiver(boolean isReceiver) {
        if(isReceiver) {
            setPrefix("MESSAGE");
        } else {
            setPrefix("SEND");
        }
    }

    public String getReceiver() {
        return actualMessage.getReceiver();
    }

    @Override
    public String send() {
        if(getPrefix().contains("SEND")) {
            return getPrefix() + ' ' + actualMessage.getReceiver() + ' ' + actualMessage.getMessage();
        } else return getPrefix() + ' ' + actualMessage.getSender() + ' ' + actualMessage.getMessage();
    }
}

package com.saxion.nl.messages.actual_mesages.users;

import com.saxion.nl.messages.Message;

public class PublicKeyMessage extends Message {

    private String key;

    public PublicKeyMessage(String key) {
        super("PUBLIC KEY","");
        this.key = key;
    }

    @Override
    public String send() {
        return getPrefix() + ' ' + getKey();
    }

    public String getKey() {
        return key;
    }
}

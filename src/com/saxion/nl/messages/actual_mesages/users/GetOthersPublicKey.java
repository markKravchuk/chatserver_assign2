package com.saxion.nl.messages.actual_mesages.users;

import com.saxion.nl.messages.Message;

public class GetOthersPublicKey extends Message {

    //GET _ PUBLIC _ KEY _ mark
    private String username;
    private String publicKey;

    public GetOthersPublicKey(String username, String publicKey) {
        super("GET PUBLIC KEY", "");
        this.username = username;
        this.publicKey = publicKey;
    }

    public GetOthersPublicKey(String username) {
        super("GET PUBLIC KEY", "");
        this.username = username;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getUsername() {
        return username;
    }

    public String getPublicKey() {
        return publicKey;
    }

    @Override
    public String send() {
        if(publicKey == null) return getPrefix() + " " + username;
        else return getPrefix() + " " + getUsername() + " " + getPublicKey();
    }
}

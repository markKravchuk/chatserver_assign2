package com.saxion.nl.messages.actual_mesages.users;

import com.saxion.nl.messages.Message;

public class BroadcastMessage extends Message {

    private String username;

    public BroadcastMessage(String username, String message) {
        super("BCST", message);
        this.username = username;
    }

    public BroadcastMessage(String message) {
        super("BCST",message);
    }

    //creating a copy constructor
    public BroadcastMessage(BroadcastMessage copy) {
        super(copy.getPrefix(),copy.getMessage());
        username = copy.getUsername();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String send() {
        if(username == null) {
            return getPrefix() + ' ' + getMessage();
        } else {
            return getPrefix() + ' ' + username + ' ' + getMessage();
        }
    }
}

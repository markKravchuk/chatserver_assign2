package com.saxion.nl.messages.actual_mesages.file;

import com.saxion.nl.messages.Message;

public class SendFileMessage extends Message {

    private String sender;
    private String receiver;
    private String filename;
    private long size;
    private boolean negativeResponse;
    private String checksum;

    public SendFileMessage(String sender, String receiver, String filename, long size) {
        super("SEND FILE", "");
        this.sender = sender;
        this.receiver = receiver;
        this.filename = filename;
        this.size = size;
    }

    public SendFileMessage(String sender, String receiver, String filename, long size, String checksum) {
        super("SEND FILE", "");
        this.sender = sender;
        this.receiver = receiver;
        this.filename = filename;
        this.size = size;
        this.checksum = checksum;
    }

    public void rejectFile() {
        setResponse(true);
        //make +OK INTO -OK
        negativeResponse = true;
        setPrefix('-' + getPrefix().substring(1));
    }

    public void swapSenderAndReceiver() {
        String temp = receiver;
        receiver = sender;
        sender = temp;
    }

    public String getChecksum() {
        return checksum;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getSender() {
        return sender;
    }

    public long getSize() {
        return size;
    }

    public String getFilename() {
        return filename;
    }

    public boolean isNegativeResponse() {
        return negativeResponse;
    }

    public void setNegativeResponse(boolean negativeResponse) {
        this.negativeResponse = negativeResponse;
    }

    @Override
    public String send() {
        if(checksum == null) {
            return getPrefix() + ' ' + sender + ' ' + receiver + ' ' + filename + ' ' + size;
        } else {
            return getPrefix() + ' ' + sender + ' ' + receiver + ' ' + filename + ' ' + size + " " + checksum;
        }
    }
}

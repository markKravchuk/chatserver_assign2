package com.saxion.nl.messages.actual_mesages.file;

import com.saxion.nl.messages.Message;

public class SendFileResponse extends Message {

    boolean correct;

    public SendFileResponse(boolean correct) {
        super("SEND FILE RESPONSE","");
        this.correct = correct;
    }

    public boolean isCorrect() {
        return correct;
    }

    @Override
    public String send() {
        return getPrefix() + " " + correct;
    }
}

package com.saxion.nl.messages.actual_mesages.groups;

import com.saxion.nl.messages.Message;

public class KickUserMessage extends Message {

    private String userToKick;
    private String group;

    public KickUserMessage(String group, String userToKick) {
        super("KICK",group + ' ' + userToKick);
        this.userToKick = userToKick;
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public String getUserToKick() {
        return userToKick;
    }

    @Override
    public String send() {
        return getPrefix() + ' ' + getMessage();
    }
}

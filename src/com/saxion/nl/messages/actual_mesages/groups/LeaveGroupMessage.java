package com.saxion.nl.messages.actual_mesages.groups;

import com.saxion.nl.messages.Message;

public class LeaveGroupMessage extends Message {

    public LeaveGroupMessage (String message) {
        super("LEAVE GROUP",message);
    }

    @Override
    public String send() {
        return getPrefix() + ' ' + getMessage();
    }
}

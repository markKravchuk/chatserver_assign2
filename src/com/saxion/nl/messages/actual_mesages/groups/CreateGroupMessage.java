package com.saxion.nl.messages.actual_mesages.groups;

import com.saxion.nl.messages.Message;

public class CreateGroupMessage extends Message {

    public CreateGroupMessage(String message) {
        super("CREATE GROUP", message);
    }

    @Override
    public String send() {
        return getPrefix() + ' ' + getMessage();
    }
}

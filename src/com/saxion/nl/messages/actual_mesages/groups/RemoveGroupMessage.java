package com.saxion.nl.messages.actual_mesages.groups;

import com.saxion.nl.messages.Message;

public class RemoveGroupMessage extends Message {

    public RemoveGroupMessage(String groupName) {
        super("REMOVE GROUP",groupName);
    }

    @Override
    public String send() {
        return getPrefix() + ' ' + getMessage();
    }
}

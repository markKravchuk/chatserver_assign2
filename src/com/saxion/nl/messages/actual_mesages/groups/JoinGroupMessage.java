package com.saxion.nl.messages.actual_mesages.groups;

import com.saxion.nl.messages.Message;

public class JoinGroupMessage extends Message {

    public JoinGroupMessage(String message) {
        super("JOIN GROUP",message);
    }


    @Override
    public String send() {
        return getPrefix() + ' ' + getMessage();
    }
}

package com.saxion.nl.messages.actual_mesages.groups;

import com.saxion.nl.messages.Message;

public class GroupMessage extends Message {

    //PROTOCOL : SEND/MESSAGE GROUP _group_ _sender_ _message_

    private String groupName;
    private String message;
    private String sender;

//    public GroupMessage(String groupName, String message) {
//        super("SEND GROUP","");
//        this.groupName = groupName;
//        this.message = message;
//    }

    public GroupMessage(String groupName, String sender, String message) {
        super("SEND GROUP","");
        this.groupName = groupName;
        this.message = message;
        this.sender = sender;
    }

    // copy constructor
    public GroupMessage(GroupMessage groupMessage) {
        super("SEND GROUP","");
        this.groupName = groupMessage.getGroupName();
        this.message = groupMessage.getMessage();
        this.sender = groupMessage.getSender();
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getGroupName() {
        return groupName;
    }


    @Override
    public String getMessage() {
        return message;
    }

    public String getSender() {
        return sender;
    }

    public void isForReceiver(boolean isReceiver) {
        if(isReceiver) {
            setPrefix("MESSAGE GROUP");
        } else {
            setPrefix("SEND GROUP");
        }
    }

    @Override
    public String send() {
        return getPrefix() + ' ' + groupName + ' ' + sender + ' ' + message;
    }
}

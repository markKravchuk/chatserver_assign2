package com.saxion.nl.messages.actual_mesages.groups;

import com.saxion.nl.messages.Message;

public class RequestGroupsMessage extends Message {

    public RequestGroupsMessage(String groups) {
        super("GET GROUPS",groups);
    }

    public RequestGroupsMessage() {
        super("GET GROUPS","");
    }

    public void fillGroups(String groups) {
        setMessage(groups);
    }

    @Override
    public String send() {
        if (getMessage().equals("")) {
            return getPrefix();
        } else {
            return getPrefix() + ' ' + getMessage();
        }
    }
}

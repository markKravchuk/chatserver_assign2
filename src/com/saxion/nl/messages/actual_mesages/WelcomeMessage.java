package com.saxion.nl.messages.actual_mesages;

import com.saxion.nl.messages.Message;

public class WelcomeMessage extends Message {

    public WelcomeMessage() {
        super("HELO","Welcome to ChatServer!");
    }

    @Override
    public String send() {
        return getPrefix() + ' ' + getMessage();
    }
}

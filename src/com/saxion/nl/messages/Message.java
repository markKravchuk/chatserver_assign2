package com.saxion.nl.messages;

public abstract class Message {

    private String prefix;
    private String message;
    private boolean isResponse;

    public Message(String prefix, String message) {
        this.prefix = prefix;
        this.message = message;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setResponse(boolean response) {
        if(response) {
            prefix = "+OK " + prefix;
            isResponse = true;
        } else {
            prefix = prefix.substring(4);
            isResponse = false;
        }
    }

    public boolean isResponse() {
        return isResponse;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public abstract String send();

}
